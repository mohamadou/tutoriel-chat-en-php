<?php
//include_once("db-connect.php");


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="css/styles.css"/>
    <title>Login</title>
</head>

<body>
<div id="conteneur">
    <div id="conteneur-form">
        <h1>Formulaire de login</h1>
        <form method="post" action="chat-room.php" id="formulaire">
            <label for="Email">Email</label>
            <input type="text" name="Email" />
            <br/>
            <label for="password">Password</label>
            <input type="text" name="Password" />
            <br/>
            <input type="submit" name="login" value="Login">
        </form>
    </div>
    <h3 id="lien"> <a href="index.php">Créer un compte</a></h3>
</div>
</body>

</html>