<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="css/styles.css"/>
    <link rel="stylesheet" href="css/chat-room.css"/>
    <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/chat-room.js"></script>
    <title>Chat-room</title>
</head>
<body>
<div id="conteneur">
    <!-- div nav header -->
    <div id="nav">Hello user</div>

    <!--Div conteneur chat window-->
    <div id="chat-windows-conteneur">
 <!--contient les chats windows-->

    </div>
    <!--users list-->
    <div id="chat-list"><h3>Liste des connectés</h3>
        <ul>
            <a href="#"><li class="users" id="1" >User 1</li></a>
            <a href="#"><li class="users" id="2"  >User 2</li></a>
           <a href="#"><li class="users" id="3" >User 3</li></a>

        </ul>
    </div>
</div>
</body>
</html>